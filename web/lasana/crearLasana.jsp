<%-- 
    Document   : crearLasaña
    Created on : 6/06/2020, 11:30:49 PM
    Author     : Harold
--%>

<%@page import="Modelo.LasanaServices"%>
<%@page import="Modelo.Lasana"%>
<%@page import="org.apache.commons.io.FilenameUtils"%>
<%@page import="java.io.*,java.util.*, javax.servlet.*" %>
<%@page import="javax.servlet.ServletInputStream" %>
<%@page import="org.apache.commons.fileupload.*" %>
<%@page import="org.apache.commons.fileupload.disk.*" %>
<%@page import="org.apache.commons.fileupload.servlet.*" %>
<%@page import="org.apache.commons.io.output.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    String name = "", desc = "", archivo="",pre="";
    String path = getServletContext().getRealPath("/")+"lasana/fotos";

    FileItemFactory factory = new DiskFileItemFactory();
    ServletFileUpload upload = new ServletFileUpload(factory);
    List items = upload.parseRequest(request);
    Iterator<FileItem> iter = items.iterator();
    while (iter.hasNext()) {
        FileItem uploaded = iter.next();

        if (uploaded.isFormField()) {
            String fieldName = uploaded.getFieldName();
            if (fieldName.equals("name")) {
                name = uploaded.getString();
            }
            if (fieldName.equals("desc")) {
                desc = uploaded.getString();
            }
            if (fieldName.equals("pre")) {
                pre = uploaded.getString();
            }

        } else {    
            if (uploaded.getSize() > 0) {
                String [] nombres = name.split(" ");
                archivo = "lasana" + nombres[1] + "." + FilenameUtils.getExtension(uploaded.getName());
                File fichero = new File(path,archivo);
                uploaded.write(fichero);
            }
        }
    }

    Lasana l = new Lasana(0,name,desc,pre,archivo);
    LasanaServices lasana = new LasanaServices();
    lasana.create(l);
    response.sendRedirect("../Dashboard/element-lasana.jsp");
%>