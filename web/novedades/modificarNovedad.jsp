<%-- 
    Document   : modificarNovedad
    Created on : 16/06/2020, 11:04:40 PM
    Author     : Harold
--%>

<%@page import="Modelo.Novedad"%>
<%@page import="Modelo.NovedadServices"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    int id = Integer.parseInt(request.getParameter("id"));
    NovedadServices n = new NovedadServices();
    Novedad novedad = n.readNovedad(id);
%>
<div class="container-fluid bcontent">
    <form action="../novedades/actualizarNovedad.jsp" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label for="text">Id</label>
            <input hidden="true" type="text" class="form-control"  id="id" name="id" value="<%=novedad.getId()%>">
        </div>
        <div class="form-group">
            <label for="text">Nombre de Novedad</label>
            <input type="text" class="form-control" id="name" name="name" value="<%=novedad.getNombre()%>">
        </div>
        <div class="form-group">
            <label for="text">Descripcion</label>
            <textarea class="form-control" id="desc" name="desc" placeholder="Ingrese Descripcion" rows="4"><%=novedad.getDescripcion()%></textarea>
        </div>
        <div class="form-group">
            <label for="text">Fecha</label>
            <input type="date" id="fecha" name="fecha" value="<%=novedad.getFecha()%>">
        </div>
        <div class="form-group">
            <label for="fileupload">Imagen</label>
            <input type="file" class="form-control-file" id="img" name="img">
        </div>
        <button type="submit" class="btn btn-primary">Actualizar</button>
        <a href="../Dashboard/element-novedad.jsp" role="button" class="btn btn-primary"> <span>Volver</span></a>
    </form>
</div>
