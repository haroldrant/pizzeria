<%-- 
    Document   : mostrarLasaña
    Created on : 6/06/2020, 11:31:59 PM
    Author     : Harold
--%>

<%@page import="Modelo.Lasana"%>
<%@page import="Modelo.LasanaServices"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    LasanaServices lasaña = new LasanaServices();
    ArrayList<Lasana> lasañas = new ArrayList();
    lasañas = lasaña.read();
    for (Lasana las : lasañas) {
%>
<div class="col-md-6 food-menu lasana">
    <div class="sided-90x mb-30 ">
        <div class="s-left"><img class="br-3" src="lasana/fotos/<%=las.getFoto()%>" alt="Menu Image"></div><!--s-left-->
        <div class="s-right">
            <h5 class="mb-10"><b><%=las.getNombre()%></b><b class="color-primary float-right">$<%=las.getPrecio()%> COP</b></h5>
            <p class="pr-70"><%=las.getDescripcion()%> </p>
        </div><!--s-right-->
    </div><!-- sided-90x -->
</div><!-- food-menu -->
<%}%>