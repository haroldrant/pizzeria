/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import bdatos.MysqlDb;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Harold
 */
public class PanzerottiServices {
    public void create(Panzerotti p) {
        MysqlDb mysqldb = new MysqlDb();
        Connection connection = mysqldb.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO panzerottis VALUES (NULL, ?,?,?,?)");
            statement.setString(1, p.getNombre());
            statement.setString(2, p.getDescripcion());
            statement.setString(3, p.getPrecio());
            statement.setString(4, p.getFoto());
            statement.executeUpdate();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Panzerotti> read() {
        ArrayList<Panzerotti> p = new ArrayList();
        MysqlDb mysqldb = new MysqlDb();
        Connection connection = mysqldb.getConnection();
        try {
            String sql = "SELECT * FROM panzerottis ORDER BY id desc";
            Statement sentencia = connection.prepareStatement(sql);
            ResultSet resultado = sentencia.executeQuery(sql);
            while (resultado.next()) {
                p.add(new Panzerotti(resultado.getInt("id"),resultado.getString("nombre"), 
                        resultado.getString("descripcion"),resultado.getString("precio"), 
                        resultado.getString("foto")));
            }
            connection.close();
            return p;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Panzerotti readPizza(int id) {
        MysqlDb mysqldb = new MysqlDb();
        Connection connection = mysqldb.getConnection();
        Panzerotti p = null;
        try {
            String sql = "SELECT * FROM panzerottis WHERE id='" + id + "'";
            Statement sentencia = connection.prepareStatement(sql);
            ResultSet resultado = sentencia.executeQuery(sql);
            while (resultado.next()) {
                p = new Panzerotti(resultado.getInt("id"),resultado.getString("nombre"),
                        resultado.getString("descripcion"),resultado.getString("precio"), 
                        resultado.getString("foto"));
            }
            connection.close();
            return p;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void update(Panzerotti p) {
        MysqlDb mysqldb = new MysqlDb();
        Connection connection = mysqldb.getConnection();
        try{
            PreparedStatement sentencia = connection.prepareStatement("UPDATE panzerottis SET nombre=?,"
                    + "descripcion=?, precio=?, foto=? WHERE id=?");
            sentencia.setString(1, p.getNombre());
            sentencia.setString(2, p.getDescripcion());
            sentencia.setString(3, p.getPrecio());
            sentencia.setString(4, p.getFoto());
            sentencia.setInt(5, p.getId());
            sentencia.executeUpdate();
            connection.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void delete(int id) {
        MysqlDb mysqldb = new MysqlDb();
        Connection connection = mysqldb.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM panzerottis WHERE id=?");
            statement.setInt(1, id);
            statement.executeUpdate();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
