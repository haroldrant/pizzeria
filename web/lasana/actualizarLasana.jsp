<%-- 
    Document   : actualizarLasaña
    Created on : 10 jun. 2020, 19:14:18
    Author     : RYZEN
--%>

<%@page import="Modelo.LasanaServices"%>
<%@page import="Modelo.Lasana"%>
<%@page import="org.apache.commons.io.FilenameUtils"%>
<%@page import="java.io.*,java.util.*, javax.servlet.*" %>
<%@page import="javax.servlet.ServletInputStream" %>
<%@page import="org.apache.commons.fileupload.*" %>
<%@page import="org.apache.commons.fileupload.disk.*" %>
<%@page import="org.apache.commons.fileupload.servlet.*" %>
<%@page import="org.apache.commons.io.output.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    int id = 0; String name = "", desc = "", archivo = "",pre="";
    Lasana l = null;
    LasanaServices lasaña = new LasanaServices();
    String path = getServletContext().getRealPath("/") + "lasana/fotos";

    FileItemFactory factory = new DiskFileItemFactory();
    ServletFileUpload upload = new ServletFileUpload(factory);
    List items = upload.parseRequest(request);
    Iterator<FileItem> iter = items.iterator();
    while (iter.hasNext()) {
        FileItem uploaded = iter.next();
        if (uploaded.isFormField()) {
            String fieldName = uploaded.getFieldName();
            if(fieldName.equals("id")){
                id = Integer.valueOf(uploaded.getString());
                l = lasaña.readLasaña(id);
            }
            if (fieldName.equals("name")) {
                name = uploaded.getString();
            }
            if (fieldName.equals("desc")) {
                desc = uploaded.getString();
            }
            if (fieldName.equals("pre")) {
                pre = uploaded.getString();
            }
        } else {
            if (uploaded.getSize() > 0) {
                String [] nombres = name.split(" ");
                archivo = "lasana" + nombres[1] + "." + FilenameUtils.getExtension(uploaded.getName());
                File fichero = new File(path, archivo);
                uploaded.write(fichero);
            }
            else{
                archivo = l.getFoto();
            }
        }
    }
    l.setNombre(name);
    l.setDescripcion(desc);
    l.setPrecio(pre);
    l.setFoto(archivo);
    lasaña.update(l);
    response.sendRedirect("../Dashboard/element-lasana.jsp");
%>