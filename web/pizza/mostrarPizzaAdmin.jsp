<%-- 
    Document   : mostrarPizzaAdmin
    Created on : 9/07/2020, 11:30:27 AM
    Author     : Harold
--%>

<%@page import="Modelo.Pizza"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Modelo.PizzaServices"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    PizzaServices pizza = new PizzaServices();
    ArrayList<Pizza> pizzas = new ArrayList();
    pizzas = pizza.read();
    for (Pizza piz : pizzas) {
%>
<div class="col-md-4">
    <!-- PANEL NO CONTROLS -->
    <div class="panel">
        <img src="../pizza/fotos/<%=piz.getFoto()%>" height="320">
        <div class="panel-heading">
            
            <h3 class="panel-title"><%=piz.getNombre()%></h3>
        </div>
        <div class="panel-body">
            <p><%=piz.getDescripcion()%></p>
        </div>
        <div class="panel-footer">
            <h5>$<%=piz.getPrecio()%> COP</h5>
            <a href="pizza-modificar.jsp?id=<%=piz.getId()%>" role="button" class="btn btn-primary">Editar</a>
            <a href="../pizza/borrarPizza.jsp?id=<%=piz.getId()%>" role="button" class="btn btn-primary">Borrar</a>
        </div>
    </div>
    <!-- END PANEL NO CONTROLS --> 
</div>
<%}%>
