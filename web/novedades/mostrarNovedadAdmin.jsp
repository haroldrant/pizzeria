<%-- 
    Document   : mostrarNovedad
    Created on : 16/06/2020, 10:30:22 PM
    Author     : Harold
--%>

<%@page import="java.time.format.DateTimeFormatter"%>
<%@page import="java.util.Locale"%>
<%@page import="Modelo.Novedad"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Modelo.NovedadServices"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    Locale spanishLocale = new Locale("es", "ES");
    String mes = "";
    NovedadServices novedad = new NovedadServices();
    ArrayList<Novedad> n = new ArrayList();
    n = novedad.read();
    for (Novedad nov : n) {
        String fecha = nov.getFecha().format(DateTimeFormatter.ofPattern("MMMM dd, yyyy", spanishLocale));
        fecha = fecha.substring(0, 1).toUpperCase() + fecha.substring(1);

%>
<div class="col-md-4">
    <!-- PANEL NO CONTROLS -->
    <div class="panel">
        <img src="../novedades/fotos/<%=nov.getFoto()%>" height="320">
        <div class="panel-heading">

            <h3 class="panel-title"><%=nov.getNombre()%></h3>
            <div><a><%=fecha%></a></div>
        </div>
        <div class="panel-body">
            <p><%=nov.getDescripcion()%></p>
        </div>
        <div class="panel-footer">
            <a href="novedad-modificar.jsp?id=<%=nov.getId()%>" role="button" class="btn btn-primary">Editar</a>
            <a href="../novedades/borrarNovedad.jsp?id=<%=nov.getId()%>" role="button" class="btn btn-primary">Borrar</a>
        </div>
    </div>
</div>
<%}%>
