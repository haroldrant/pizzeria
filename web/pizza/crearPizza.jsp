<%-- 
    Document   : crearPizza
    Created on : 29/05/2020, 07:54:31 PM
    Author     : Harold
--%>
<%@page import="Modelo.PizzaServices"%>
<%@page import="Modelo.Pizza"%>
<%@page import="org.apache.commons.io.FilenameUtils"%>
<%@page import="java.io.*,java.util.*, javax.servlet.*" %>
<%@page import="javax.servlet.ServletInputStream" %>
<%@page import="org.apache.commons.fileupload.*" %>
<%@page import="org.apache.commons.fileupload.disk.*" %>
<%@page import="org.apache.commons.fileupload.servlet.*" %>
<%@page import="org.apache.commons.io.output.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    String name = "", desc = "", archivo = "",pre="";
    String path = getServletContext().getRealPath("/") + "pizza/fotos";

    FileItemFactory factory = new DiskFileItemFactory();
    ServletFileUpload upload = new ServletFileUpload(factory);
    List items = upload.parseRequest(request);
    Iterator<FileItem> iter = items.iterator();
    while (iter.hasNext()) {
        FileItem uploaded = iter.next();
        if (uploaded.isFormField()) {
            String fieldName = uploaded.getFieldName();
            if (fieldName.equals("name")) {
                name = uploaded.getString();
            }
            if (fieldName.equals("desc")) {
                desc = uploaded.getString();
            }
            if (fieldName.equals("pre")) {
                pre = uploaded.getString();
            }
        } else {
            if (uploaded.getSize() > 0) {
                archivo = name + "." + FilenameUtils.getExtension(uploaded.getName());
                File fichero = new File(path, archivo);
                uploaded.write(fichero);
            }
        }
    }

    Pizza p = new Pizza(0, name, desc, pre, archivo);
    PizzaServices piz = new PizzaServices();
    piz.create(p);
    response.sendRedirect("../Dashboard/element-pizza.jsp");
%>