<%-- 
    Document   : borrarPasta
    Created on : 10/06/2020, 11:31:29 PM
    Author     : Harold
--%>

<%@page import="java.io.File"%>
<%@page import="Modelo.Pasta"%>
<%@page import="Modelo.PastaServices"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    int id = Integer.valueOf(request.getParameter("id"));
    PastaServices p = new PastaServices();
    Pasta pasta = p.readPasta(id);
    String path = getServletContext().getRealPath("/") + "pasta/fotos";
    File file = new File(path,pasta.getFoto());
    file.delete();
    p.delete(id);
    response.sendRedirect("mostrarPasta.jsp");
%>
