/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import bdatos.MysqlDb;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Harold
 */
public class PastaServices {
    public void create(Pasta p) {
        MysqlDb mysqldb = new MysqlDb();
        Connection connection = mysqldb.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO pastas VALUES (NULL, ?,?,?,?)");
            statement.setString(1, p.getNombre());
            statement.setString(2, p.getDescripcion());
            statement.setString(3, p.getPrecio());
            statement.setString(4, p.getFoto());
            statement.executeUpdate();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public ArrayList<Pasta> read() {
        ArrayList<Pasta> pasta = new ArrayList();
        MysqlDb mysqldb = new MysqlDb();
        Connection connection = mysqldb.getConnection();
        try {
            String sql = "SELECT * FROM pastas ORDER BY id desc";
            Statement sentencia = connection.prepareStatement(sql);
            ResultSet resultado = sentencia.executeQuery(sql);
            while (resultado.next()) {
                pasta.add(new Pasta(resultado.getInt("id"),resultado.getString("nombre"), resultado.getString("descripcion"),
                resultado.getString("precio"), resultado.getString("foto")));
            }
            connection.close();
            return pasta;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public Pasta readPasta(int id) {
        MysqlDb mysqldb = new MysqlDb();
        Connection connection = mysqldb.getConnection();
        Pasta p = null;
        try {
            String sql = "SELECT * FROM pastas WHERE id='" + id + "'";
            Statement sentencia = connection.prepareStatement(sql);
            ResultSet resultado = sentencia.executeQuery(sql);
            while (resultado.next()) {
                p = new Pasta(resultado.getInt("id"),resultado.getString("nombre"),
                        resultado.getString("descripcion"),resultado.getString("precio"), 
                        resultado.getString("foto"));
            }
            connection.close();
            return p;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public void update(Pasta p) {
        MysqlDb mysqldb = new MysqlDb();
        Connection connection = mysqldb.getConnection();
        try{
            PreparedStatement sentencia = connection.prepareStatement("UPDATE pastas SET nombre=?,"
                    + "descripcion=?, precio=?, foto=? WHERE id=?");
            sentencia.setString(1, p.getNombre());
            sentencia.setString(2, p.getDescripcion());
            sentencia.setString(3, p.getPrecio());
            sentencia.setString(4, p.getFoto());
            sentencia.setInt(5, p.getId());
            sentencia.executeUpdate();
            connection.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public void delete(int id) {
        MysqlDb mysqldb = new MysqlDb();
        Connection connection = mysqldb.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM pastas WHERE id=?");
            statement.setInt(1, id);
            statement.executeUpdate();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
