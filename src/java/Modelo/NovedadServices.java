/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import bdatos.MysqlDb;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author Harold
 */
public class NovedadServices {
    
    public void create(Novedad n) {
        MysqlDb mysqldb = new MysqlDb();
        Connection connection = mysqldb.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO novedades VALUES (NULL, ?,?,?,?)");
            Date date = Date.valueOf(n.getFecha().toString());
            statement.setString(1, n.getNombre());
            statement.setString(2, n.getDescripcion());
            statement.setDate(3, date);
            statement.setString(4, n.getFoto());
            statement.executeUpdate();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public ArrayList<Novedad> read() {
        ArrayList<Novedad> novedad = new ArrayList();
        MysqlDb mysqldb = new MysqlDb();
        Connection connection = mysqldb.getConnection();
        try {
            String sql = "SELECT * FROM novedades ORDER BY id desc";
            Statement sentencia = connection.prepareStatement(sql);
            ResultSet resultado = sentencia.executeQuery(sql);
            while (resultado.next()) {
                LocalDate date = resultado.getDate("fecha").toLocalDate();
                novedad.add(new Novedad(resultado.getInt("id"),resultado.getString("nombre"), resultado.getString("descripcion"),
                date, resultado.getString("foto")));
            }
            connection.close();
            return novedad;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public Novedad readNovedad(int id) {
        MysqlDb mysqldb = new MysqlDb();
        Connection connection = mysqldb.getConnection();
        Novedad n = null;
        try {
            String sql = "SELECT * FROM novedades WHERE id='" + id + "'";
            Statement sentencia = connection.prepareStatement(sql);
            ResultSet resultado = sentencia.executeQuery(sql);
            while (resultado.next()) {
                LocalDate date = resultado.getDate("fecha").toLocalDate();
                n = new Novedad(resultado.getInt("id"),resultado.getString("nombre"), 
                        resultado.getString("descripcion"),date, resultado.getString("foto"));
            }
            connection.close();
            return n;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public void update(Novedad n) {
        MysqlDb mysqldb = new MysqlDb();
        Connection connection = mysqldb.getConnection();
        try{
            PreparedStatement sentencia = connection.prepareStatement("UPDATE novedades SET nombre=?,"
                    + "descripcion=?, fecha=?, foto=? WHERE id=?");
            Date date = Date.valueOf(n.getFecha().toString());
            sentencia.setString(1, n.getNombre());
            sentencia.setString(2, n.getDescripcion());
            sentencia.setDate(3, date);
            sentencia.setString(4, n.getFoto());
            sentencia.setInt(5, n.getId());
            sentencia.executeUpdate();
            connection.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public void delete(int id) {
        MysqlDb mysqldb = new MysqlDb();
        Connection connection = mysqldb.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM novedades WHERE id=?");
            statement.setInt(1, id);
            statement.executeUpdate();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
