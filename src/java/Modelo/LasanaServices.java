/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import bdatos.MysqlDb;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Chozkar
 */
public class LasanaServices {

    public void create(Lasana l) {
        MysqlDb mysqldb = new MysqlDb();
        Connection connection = mysqldb.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO lasanas VALUES (NULL, ?,?,?,?)");
            statement.setString(1, l.getNombre());
            statement.setString(2, l.getDescripcion());
            statement.setString(3, l.getPrecio());
            statement.setString(4, l.getFoto());
            statement.executeUpdate();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public ArrayList<Lasana> read() {
        ArrayList<Lasana> lasaña = new ArrayList();
        MysqlDb mysqldb = new MysqlDb();
        Connection connection = mysqldb.getConnection();
        try {
            String sql = "SELECT * FROM lasanas ORDER BY id desc";
            Statement sentencia = connection.prepareStatement(sql);
            ResultSet resultado = sentencia.executeQuery(sql);
            while (resultado.next()) {
                lasaña.add(new Lasana(resultado.getInt("id"),resultado.getString("nombre"), resultado.getString("descripcion"),
                resultado.getString("precio"), resultado.getString("foto")));
            }
            connection.close();
            return lasaña;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    

    public Lasana readLasaña(int id) {
        MysqlDb mysqldb = new MysqlDb();
        Connection connection = mysqldb.getConnection();
        Lasana l = null;
        try {
            String sql = "SELECT * FROM lasanas WHERE id='" + id + "'";
            Statement sentencia = connection.prepareStatement(sql);
            ResultSet resultado = sentencia.executeQuery(sql);
            while (resultado.next()) {
                l = new Lasana(resultado.getInt("id"),resultado.getString("nombre"),
                        resultado.getString("descripcion"),resultado.getString("precio"), 
                        resultado.getString("foto"));
            }
            connection.close();
            return l;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void update(Lasana l) {
        MysqlDb mysqldb = new MysqlDb();
        Connection connection = mysqldb.getConnection();
        try{
            PreparedStatement sentencia = connection.prepareStatement("UPDATE lasanas SET nombre=?,"
                    + "descripcion=?, precio=?, foto=? WHERE id=?");
            sentencia.setString(1, l.getNombre());
            sentencia.setString(2, l.getDescripcion());
            sentencia.setString(3, l.getPrecio());
            sentencia.setString(4, l.getFoto());
            sentencia.setInt(5, l.getId());
            sentencia.executeUpdate();
            connection.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void delete(int id) {
        MysqlDb mysqldb = new MysqlDb();
        Connection connection = mysqldb.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM lasanas WHERE id=?");
            statement.setInt(1, id);
            statement.executeUpdate();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}