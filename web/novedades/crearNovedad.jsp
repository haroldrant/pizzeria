<%@page import="java.time.LocalDate"%>

<div class="container-fluid bcontent">
    <form action="../novedades/crearNov.jsp" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label for="text">Nombre Novedad</label>
            <input type="text" class="form-control" id="name" name="name" placeholder="Ingrese nombre">
        </div>
        <div class="form-group">
            <label for="text">Descripcion</label>
            <textarea class="form-control" id="desc" name="desc" placeholder="Ingrese Descripcion" rows="4"></textarea>
        </div>
        <%
            LocalDate fecha = LocalDate.now();
        %>
        <div class="form-group">
            <label for="text">Fecha</label>
            <input type="date" id="fecha" name="fecha" value="<%=fecha%>">
        </div>
        <div class="form-group">
            <label for="fileupload">Imagen</label>
            <input type="file" class="form-control-file" id="img" name="img">
        </div>
        <button type="submit" class="btn btn-primary">Crear</button>
        <a href="../Dashboard/element-novedad.jsp" role="button" class="btn btn-primary"> <span>Volver</span></a>
    </form>
</div>
