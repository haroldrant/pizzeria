<%-- 
    Document   : borrarPizza
    Created on : 9/06/2020, 06:39:39 PM
    Author     : Harold
--%>

<%@page import="java.io.File"%>
<%@page import="Modelo.Pizza"%>
<%@page import="Modelo.PizzaServices"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    int id = Integer.valueOf(request.getParameter("id"));
    PizzaServices p = new PizzaServices();
    Pizza pizza = p.readPizza(id);
    String path = getServletContext().getRealPath("/") + "pizza/fotos";
    File file = new File(path,pizza.getFoto());
    file.delete();
    p.delete(id);
    response.sendRedirect("../Dashboard/element-pizza.jsp");
%>