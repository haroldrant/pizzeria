/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import bdatos.MysqlDb;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Harold
 */
public class PizzaServices {

    public void create(Pizza p) {
        MysqlDb mysqldb = new MysqlDb();
        Connection connection = mysqldb.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO pizzas VALUES (NULL, ?,?,?,?)");
            statement.setString(1, p.getNombre());
            statement.setString(2, p.getDescripcion());
            statement.setString(3, p.getPrecio());
            statement.setString(4, p.getFoto());
            statement.executeUpdate();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Pizza> read() {
        ArrayList<Pizza> pizza = new ArrayList();
        MysqlDb mysqldb = new MysqlDb();
        Connection connection = mysqldb.getConnection();
        try {
            String sql = "SELECT * FROM pizzas ORDER BY id desc";
            Statement sentencia = connection.prepareStatement(sql);
            ResultSet resultado = sentencia.executeQuery(sql);
            while (resultado.next()) {
                pizza.add(new Pizza(resultado.getInt("id"),resultado.getString("nombre"), 
                        resultado.getString("descripcion"),resultado.getString("precio"), 
                        resultado.getString("foto")));
            }
            connection.close();
            return pizza;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Pizza readPizza(int id) {
        MysqlDb mysqldb = new MysqlDb();
        Connection connection = mysqldb.getConnection();
        Pizza p = null;
        try {
            String sql = "SELECT * FROM pizzas WHERE id='" + id + "'";
            Statement sentencia = connection.prepareStatement(sql);
            ResultSet resultado = sentencia.executeQuery(sql);
            while (resultado.next()) {
                p = new Pizza(resultado.getInt("id"),resultado.getString("nombre"),
                        resultado.getString("descripcion"),resultado.getString("precio"), 
                        resultado.getString("foto"));
            }
            connection.close();
            return p;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void update(Pizza p) {
        MysqlDb mysqldb = new MysqlDb();
        Connection connection = mysqldb.getConnection();
        try{
            PreparedStatement sentencia = connection.prepareStatement("UPDATE pizzas SET nombre=?,"
                    + "descripcion=?, precio=?, foto=? WHERE id=?");
            sentencia.setString(1, p.getNombre());
            sentencia.setString(2, p.getDescripcion());
            sentencia.setString(3, p.getPrecio());
            sentencia.setString(4, p.getFoto());
            sentencia.setInt(5, p.getId());
            sentencia.executeUpdate();
            connection.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void delete(int id) {
        MysqlDb mysqldb = new MysqlDb();
        Connection connection = mysqldb.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM pizzas WHERE id=?");
            statement.setInt(1, id);
            statement.executeUpdate();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}