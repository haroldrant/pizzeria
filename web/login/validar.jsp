<%@page import="Modelo.InicioSesion"%>
<%@page session="true"%>
<%
    String usuario = request.getParameter("usuario");
    String pass = request.getParameter("contra");
    InicioSesion iniciar = new InicioSesion();
    if (iniciar.iniciarSesion(usuario, pass)) {%>
        <%
            HttpSession sesion = request.getSession();
            sesion.setAttribute("user", usuario);
        %>
        <meta http-equiv="refresh" content="0.1;URL=../Dashboard/index.jsp"/>
<%}else{    
    if(request.getParameter("cerrar")!=null){%>
        <meta http-equiv="refresh" content="0.1;URL=sesionFinalizada.html"/>
        <%
        session.invalidate();
    }else{%>
        <meta http-equiv="refresh" content="0.1;URL=sesionError.html"/>
<%    
    }}
%>