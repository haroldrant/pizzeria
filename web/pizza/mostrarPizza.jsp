<%-- 
    Document   : mostrarAutor
    Created on : 26/05/2020, 11:41:13 p. m.
    Author     : Harold
--%>

<%@page import="Modelo.Pizza"%>
<%@page import="Modelo.PizzaServices"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<%
    PizzaServices pizza = new PizzaServices();
    ArrayList<Pizza> pizzas = new ArrayList();
    pizzas = pizza.read();
    for (Pizza piz : pizzas) {
%>
<div class="col-md-6 food-menu pizza">
    <div class="sided-90x mb-30 ">
        <div class="s-left"><img class="br-3" src="pizza/fotos/<%=piz.getFoto()%>" alt="Menu Image"></div><!--s-left-->
        <div class="s-right">
            <h5 class="mb-10"><b><%=piz.getNombre()%></b><b class="color-primary float-right">$<%=piz.getPrecio()%> COP</b></h5>
            <h5 class="mt-20"><%=piz.getDescripcion()%> </h5>
        </div><!--s-right-->
    </div><!-- sided-90x -->
</div><!-- food-menu -->
<%}%>

