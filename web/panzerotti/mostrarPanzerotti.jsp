<%-- 
    Document   : mostrarAutor
    Created on : 26/05/2020, 11:41:13 p. m.
    Author     : Harold
--%>

<%@page import="Modelo.Panzerotti"%>
<%@page import="Modelo.PanzerottiServices"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    PanzerottiServices panzerotti = new PanzerottiServices();
    ArrayList<Panzerotti> panzerottis = new ArrayList();
    panzerottis = panzerotti.read();
    for (Panzerotti piz : panzerottis) {
%>
<div class="col-md-6 food-menu panzerotti">
    <div class="sided-90x mb-30">
        <div class="s-left"><img class="br-3" src="panzerotti/fotos/<%=piz.getFoto()%>" alt="Menu Image"></div><!--s-left-->
        <div class="s-right">
            <h5 class="mb-10"><b><%=piz.getNombre()%></b><b class="color-primary float-right">$<%=piz.getPrecio()%> COP</b></h5>
            <p class="pr-70"><%=piz.getDescripcion()%></p>
        </div><!--s-right-->
    </div><!-- sided-90x -->
</div><!-- food-menu -->
<%}%>