<%-- 
    Document   : modificarPasta
    Created on : 10/06/2020, 11:36:11 PM
    Author     : Harold
--%>

<%@page import="Modelo.Pasta"%>
<%@page import="Modelo.PastaServices"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    int id = Integer.parseInt(request.getParameter("id"));
    PastaServices pasta = new PastaServices();
    Pasta p = pasta.readPasta(id);
%>
<div class="container-fluid bcontent">
    <form action="../actualizarPasta.jsp" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label for="text">Id</label>
            <input hidden="true" type="text" class="form-control"  id="id" name="id" value="<%=p.getId()%>">
        </div>
        <div class="form-group">
            <label for="text">Nombre de Pasta</label>
            <input type="text" class="form-control" id="name" name="name" value="<%=p.getNombre()%>">
        </div>
        <div class="form-group">
            <label for="text">Descripcion</label>
            <textarea class="form-control" id="desc" name="desc" rows="4"><%=p.getDescripcion()%></textarea>
        </div>
        <div class="form-group">
            <label for="text">Precio</label>
            <div class="input-group">
                <span class="input-group-addon">$</span>
                <input class="form-control" type="text" id="pre" name="pre" value="<%=p.getPrecio() %>">
                <span class="input-group-addon">.00</span>
            </div>
        </div>
        <div class="form-group">
            <label for="fileupload">Imagen</label>
            <input type="file" class="form-control-file" id="img" name="img">
        </div>
        <button type="submit" class="btn btn-primary">Actualizar</button>
        <a href="../Dashboard/element-pasta.jsp" role="button" class="btn btn-primary"> <span>Volver</span></a>
    </form>
</div>
