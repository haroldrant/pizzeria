<%-- 
    Document   : modificarLasaña
    Created on : 10 jun. 2020, 19:18:12
    Author     : RYZEN
--%>

<%@page import="Modelo.LasanaServices"%>
<%@page import="Modelo.Lasana"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    int id = Integer.parseInt(request.getParameter("id"));
    LasanaServices l = new LasanaServices();
    Lasana lasana = l.readLasaña(id);
%>
<div class="container-fluid bcontent">
    <form action="../lasana/actualizarLasana.jsp" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label for="text">Id</label>
            <input hidden="true" type="text" class="form-control"  id="id" name="id" value="<%=lasana.getId()%>">
        </div>
        <div class="form-group">
            <label for="text">Nombre de Lasaña</label>
            <input type="text" class="form-control" id="name" name="name" value="<%=lasana.getNombre()%>">
        </div>
        <div class="form-group">
            <label for="text">Descripcion</label>                    
            <textarea class="form-control" id="desc" name="desc" rows="4"><%=lasana.getDescripcion()%></textarea>
        </div>
        <div class="form-group">
            <label for="text">Precio</label>
            <div class="input-group">
                <span class="input-group-addon">$</span>
                <input class="form-control" type="text" id="pre" name="pre" value="<%=lasana.getPrecio()%>">
                <span class="input-group-addon">.00</span>
            </div>
        </div>
        <div class="form-group">
            <label for="fileupload">Imagen</label>
            <input type="file" class="form-control-file" id="img" name="img">
        </div>
        <button type="submit" class="btn btn-primary">Actualizar</button>
        <a href="../Dashboard/element-lasana.jsp" role="button" class="btn btn-primary"> <span>Volver</span></a>
    </form>
</div>