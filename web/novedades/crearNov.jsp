<%-- 
    Document   : crearNov
    Created on : 16/06/2020, 04:34:09 PM
    Author     : Harold
--%>
<%@page import="Modelo.NovedadServices"%>
<%@page import="Modelo.Novedad"%>
<%@page import="java.time.LocalDate"%>
<%@page import="org.apache.commons.io.FilenameUtils"%>
<%@page import="java.io.*,java.util.*, javax.servlet.*" %>
<%@page import="javax.servlet.ServletInputStream" %>
<%@page import="org.apache.commons.fileupload.*" %>
<%@page import="org.apache.commons.fileupload.disk.*" %>
<%@page import="org.apache.commons.fileupload.servlet.*" %>
<%@page import="org.apache.commons.io.output.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    String name = "", desc = "", archivo = "";
    LocalDate fecha =null;
    String path = getServletContext().getRealPath("/") + "../../web/novedades/fotos";

    FileItemFactory factory = new DiskFileItemFactory();
    ServletFileUpload upload = new ServletFileUpload(factory);
    List items = upload.parseRequest(request);
    Iterator<FileItem> iter = items.iterator();
    while (iter.hasNext()) {
        FileItem uploaded = iter.next();
        if (uploaded.isFormField()) {
            String fieldName = uploaded.getFieldName();
            if (fieldName.equals("name")) {
                name = uploaded.getString();
            }
            if (fieldName.equals("desc")) {
                desc = uploaded.getString();
            }
            if (fieldName.equals("fecha")) {
                fecha = LocalDate.parse(uploaded.getString());
            }
        } else {
            if (uploaded.getSize() > 0) {
                String [] nombres = name.split(" ");
                archivo = nombres[0] + nombres[1] + "." + FilenameUtils.getExtension(uploaded.getName());
                File fichero = new File(path, archivo);
                uploaded.write(fichero);
            }
        }
    }
    

    Novedad n = new Novedad(0, name, desc, fecha, archivo);
    NovedadServices nov = new NovedadServices();
    nov.create(n);
    response.sendRedirect("../Dashboard/element-novedad.jsp");
%>