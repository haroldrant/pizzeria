<%-- 
    Document   : actualizarPizza
    Created on : 9/06/2020, 09:10:05 PM
    Author     : Harold
--%>
<%@page import="Modelo.PizzaServices"%>
<%@page import="Modelo.Pizza"%>
<%@page import="org.apache.commons.io.FilenameUtils"%>
<%@page import="java.io.*,java.util.*, javax.servlet.*" %>
<%@page import="javax.servlet.ServletInputStream" %>
<%@page import="org.apache.commons.fileupload.*" %>
<%@page import="org.apache.commons.fileupload.disk.*" %>
<%@page import="org.apache.commons.fileupload.servlet.*" %>
<%@page import="org.apache.commons.io.output.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    int id = 0; String name = "", desc = "", archivo = "",pre="";
    Pizza p = null;
    PizzaServices pizza = new PizzaServices();
    String path = getServletContext().getRealPath("/") + "pizza/fotos";

    FileItemFactory factory = new DiskFileItemFactory();
    ServletFileUpload upload = new ServletFileUpload(factory);
    List items = upload.parseRequest(request);
    Iterator<FileItem> iter = items.iterator();
    while (iter.hasNext()) {
        FileItem uploaded = iter.next();
        if (uploaded.isFormField()) {
            String fieldName = uploaded.getFieldName();
            if(fieldName.equals("id")){
                id = Integer.valueOf(uploaded.getString());
                p = pizza.readPizza(id);
            }
            if (fieldName.equals("name")) {
                name = uploaded.getString();
            }
            if (fieldName.equals("desc")) {
                desc = uploaded.getString();
            }
            if (fieldName.equals("pre")) {
                pre = uploaded.getString();
            }
        } else {
            if (uploaded.getSize() > 0) {
                File file = new File(path,p.getFoto());
                file.delete();
                archivo = name + "." + FilenameUtils.getExtension(uploaded.getName());
                File fichero = new File(path, archivo);
                uploaded.write(fichero);
            }
            else{
                archivo = p.getFoto();
            }
        }
    }
    p.setNombre(name);
    p.setDescripcion(desc);
    p.setPrecio(pre);
    p.setFoto(archivo);
    pizza.update(p);
    response.sendRedirect("../Dashboard/element-pizza.jsp");
%>