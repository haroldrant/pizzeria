<%-- 
    Document   : mostrarPizzaAdmin
    Created on : 9/07/2020, 11:30:27 AM
    Author     : Harold
--%>

<%@page import="Modelo.Lasana"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Modelo.LasanaServices"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    LasanaServices lasaña = new LasanaServices();
    ArrayList<Lasana> lasañas = new ArrayList();
    lasañas = lasaña.read();
    for (Lasana las : lasañas) {
%>
<div class="col-md-4">
    <!-- PANEL NO CONTROLS -->
    <div class="panel">
        <img src="../lasana/fotos/<%=las.getFoto()%>" height="320">
        <div class="panel-heading">
            
            <h3 class="panel-title"><%=las.getNombre()%></h3>
        </div>
        <div class="panel-body">
            <p><%=las.getDescripcion()%></p>
        </div>
        <div class="panel-footer">
            <h5>$<%=las.getPrecio() %> COP</h5>
            <a href="lasana-modificar.jsp?id=<%=las.getId()%>" role="button" class="btn btn-primary">Editar</a>
            <a href="../lasana/borrarLasana.jsp?id=<%=las.getId()%>" role="button" class="btn btn-primary">Borrar</a>
        </div>
    </div>
    <!-- END PANEL NO CONTROLS --> 
</div>
<%}%>
