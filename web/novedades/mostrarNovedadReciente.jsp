<%-- 
    Document   : mostrarNovedadReciente
    Created on : 25/06/2020, 08:39:59 AM
    Author     : Harold
--%>

<%@page import="java.time.LocalDate"%>
<%@page import="java.time.format.DateTimeFormatter"%>
<%@page import="Modelo.Novedad"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Modelo.NovedadServices"%>
<%@page import="java.util.Locale"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    Locale spanishLocale = new Locale("es", "ES");
    String mes = "";
    NovedadServices novedad = new NovedadServices();
    ArrayList<Novedad> n = new ArrayList();
    n = novedad.read();
    int i = 0;
    while (n.size() > i && i < 4) {
        String fecha = n.get(i).getFecha().format(DateTimeFormatter.ofPattern("MMMM dd, yyyy", spanishLocale));
        fecha = fecha.substring(0, 1).toUpperCase() + fecha.substring(1);

%>
<div class="col-md-6 food-menu pizza">
    <div class="sided-90x mb-30 ">
        <div class="s-left"><img class="br-3" src="novedades/fotos/<%=n.get(i).getFoto()%>" alt="Menu Image"></div><!--s-left-->
        <div class="s-right">
            <h5 class="mb-10"><b><%=n.get(i).getNombre()%><br></b>
                <b class="color-primary"><%=fecha%></b></h5>            
            <h5 class="mt-20"><%=n.get(i).getDescripcion()%> </h5>
        </div><!--s-right-->
    </div><!-- sided-90x -->
</div><!-- food-menu -->
<%i++;
        }%>