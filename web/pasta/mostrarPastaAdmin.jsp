<%-- 
    Document   : mostrarPasta
    Created on : 6/06/2020, 11:28:21 PM
    Author     : Harold
--%>
<%@page import="Modelo.Pasta"%>
<%@page import="Modelo.PastaServices"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    PastaServices pasta = new PastaServices();
    ArrayList<Pasta> pastas = new ArrayList();
    pastas = pasta.read();
    for (Pasta pas : pastas) {
%>
<div class="col-md-4">
    <!-- PANEL NO CONTROLS -->
    <div class="panel">
        <img src="../pasta/fotos/<%=pas.getFoto()%>" height="320">
        <div class="panel-heading">

            <h3 class="panel-title"><%=pas.getNombre()%></h3>
        </div>
        <div class="panel-body">
            <p><%=pas.getDescripcion()%></p>
        </div>
        <div class="panel-footer">
            <h5>$<%=pas.getPrecio() %> COP</h5>
            <a href="pasta-modificar.jsp?id=<%=pas.getId()%>" role="button" class="btn btn-primary">Editar</a>
            <a href="../pasta/borrarPasta.jsp?id=<%=pas.getId()%>" role="button" class="btn btn-primary">Borrar</a>
        </div>
    </div>
</div>
<!-- END PANEL NO CONTROLS -->
<%}%>