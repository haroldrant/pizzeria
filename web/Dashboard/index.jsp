<%@page session="true"%>
<!doctype html>
<html lang="en">
    <head>
        <title>Dashboard</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <!-- VENDOR CSS -->
        <link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/vendor/linearicons/style.css">
        <link rel="stylesheet" href="assets/vendor/chartist/css/chartist-custom.css">
        <!-- MAIN CSS -->
        <link rel="stylesheet" href="assets/css/main.css">
        <!-- GOOGLE FONTS -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
        <!-- ICONS -->
        <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
        <link rel="icon" href="assets/img/logo.png"/>
    </head>

    <body>
        <%
            HttpSession sesion = request.getSession();
            String usuario = "";
            if (sesion.getAttribute("user") == null) {
                response.sendRedirect("index.jsp");
            }
            else{
                sesion.setMaxInactiveInterval(60*10);
                usuario = (String)sesion.getAttribute("usuario");
            }
            
        %>
        <!-- WRAPPER -->
        <div id="wrapper">
            <!-- NAVBAR -->
            <nav class="navbar navbar-default navbar-fixed-top">
                <div class="brand">
                    <a href="index.jsp"><img src="assets/img/logo1.png" alt="Klorofil Logo" class="img-responsive logo"></a>
                </div>
                <div class="container-fluid">
                    <div class="navbar-btn">
                        <button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
                    </div>
                    <div class="navbar-btn navbar-btn-right">
                        <a href="../login/validar.jsp?cerrar=true"><i class="lnr lnr-exit"></i> <span>Cerrar Sesi�n</span></a>
                    </div>
                </div>

            </nav>
            <!-- END NAVBAR -->
            <!-- LEFT SIDEBAR -->
            <div id="sidebar-nav" class="sidebar">
                <div class="sidebar-scroll">
                    <nav>
                        <ul class="nav">
                            <li><a href="index.jsp" class="active"><i class="lnr lnr-home"></i> <span>Inicio</span></a></li>
                            <li>
                                <a href="#subPages" data-toggle="collapse" class="collapsed"><i class="fa fa-database"></i> <span>Elementos</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                                <div id="subPages" class="collapse ">
                                    <ul class="nav">
                                        <li><a href="element-pizza.jsp" class="">Pizza</a></li>
                                        <li><a href="element-lasana.jsp" class="">Lasa�a</a></li>
                                        <li><a href="element-pasta.jsp" class="">Pasta</a></li>
                                        <li><a href="element-panzerotti.jsp" class="">Panzerotti</a></li>
                                        <li><a href="element-novedad.jsp" class="">Novedad</a></li>
                                    </ul>
                                </div>
                            </li>                            
                        </ul>
                    </nav>
                </div>
            </div>
            <!-- END LEFT SIDEBAR -->
            <!-- MAIN -->
            <div class="main">
                <!-- MAIN CONTENT -->
                <div class="main-content">
                    <div class="container-fluid">
                        <!-- OVERVIEW -->
                        <div class="panel panel-headline">
                            <div class="panel-heading">
                                <h3 class="panel-title">Bienvenido a Administraci�n de Pizza Bohemia</h3>
                                <p class="panel-subtitle">Donde podra agregar nuevos productos y novedades</p>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-3">
                                            <p>
                                                <a href="element-pizza.jsp" type="button" class="btn btn-primary btn-lg">Pizza</a>
                                            </p>
                                    </div>
                                    <div class="col-md-3">
                                            <p>
                                                <a href="element-lasana.jsp" type="button" class="btn btn-primary btn-lg">Lasa�a</a>
                                            </p>
                                    </div>
                                    <div class="col-md-3">
                                            <p>
                                                <a href="element-pasta.jsp" type="button" class="btn btn-primary btn-lg">Pasta</a>
                                            </p>
                                    </div>
                                    <div class="col-md-3">
                                            <p>
                                                <a href="element-pasta.jsp" type="button" class="btn btn-primary btn-lg">Panzerotti</a>
                                            </p>
                                    </div>
                                    <div class="col-md-3">
                                            <p>
                                                <a href="element-novedad.jsp" type="button" class="btn btn-primary btn-lg">Novedades</a>
                                            </p>
                                    </div>
                                </div>
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN -->
        <div class="clearfix"></div>
        <footer>
            <div class="container-fluid">
                <p class="copyright">&copy; <script>document.write(new Date().getFullYear());</script> Todos los derechos reservados - Pizza Bohemia.</p>
            </div>
        </footer>
    </div>
    <!-- END WRAPPER -->
    <!-- Javascript -->
    <script src="assets/vendor/jquery/jquery.min.js"></script>
    <script src="assets/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <script src="assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
    <script src="assets/vendor/chartist/js/chartist.min.js"></script>
    <script src="assets/scripts/klorofil-common.js"></script>        
</body>
</html>