<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Pizza Bohemia</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="UTF-8">

        <!-- Font -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
        <link rel="stylesheet" href="fonts/beyond_the_mountains-webfont.css" type="text/css"/>

        <!-- Stylesheets -->
        <link href="plugin-frameworks/bootstrap.min.css" rel="stylesheet">
        <link href="plugin-frameworks/swiper.css" rel="stylesheet">
        <link href="fonts/ionicons.css" rel="stylesheet">
        <link href="common/styles.css" rel="stylesheet">
        <link rel="icon" href="images/logo.jpg"/>
    </head>
    <body>

        <header>
            <div class="container">
                <a class="logo" href="index.jsp"><img src="images/logo-white.png" alt="Logo"></a>

                <div class="right-area">
                    <h6><a class="plr-20 color-white btn-fill-primary" href="https://api.whatsapp.com/send?phone=573058126707">ORDENA: +57 305 812 6707</a></h6>
                </div><!-- right-area -->

                <a class="menu-nav-icon" data-menu="#main-menu" href="#"><i class="ion-navicon"></i></a>

                <ul class="main-menu font-mountainsre" id="main-menu">
                    <li><a href="index.jsp">INICIO</a></li>
                    <li><a href="menu.jsp">MEN�</a></li>
                    <li><a href="novedades.jsp">NOVEDADES</a></li>
                </ul>
                <div class="clearfix"></div>
            </div><!-- container -->
        </header>


        <section class="bg-5 h-500x main-slider pos-relative">
            <div class="triangle-up pos-bottom"></div>
            <div class="container h-100">
                <div class="dplay-tbl">
                    <div class="dplay-tbl-cell center-text color-white pt-90">
                        <h5><b>CASA DE LA PIZZABURGUER</b></h5>
                        <h2 class="mt-30 mb-15">Men�</h2>
                    </div><!-- dplay-tbl-cell -->
                </div><!-- dplay-tbl -->
            </div><!-- container -->
        </section>


        <section>            
            <div class="pos-top triangle-bottom"></div>
            <div class="triangle-up pos-bottom"></div>
            <div class="container">
                <div class="heading">
                    <img class="heading-img" src="images/logo.jpg" alt="">
                    <h2>Nuestro Men�</h2>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <ul class="selecton brdr-b-primary mb-70">
                            <li><a class="active" href="#" data-select="*"><b>TODO</b></a></li>
                            <li><a href="#" data-select="pizza"><b>PIZZA</b></a></li>
                            <li><a href="#" data-select="pasta"><b>PASTA</b></a></li>
                            <li><a href="#" data-select="lasana"><b>LASA�A</b></a></li>
                            <li><a href="#" data-select="panzerotti"><b>PANZEROTTI</b></a></li>
                        </ul>
                    </div><!--col-sm-12-->
                </div><!--row-->

                <div class="row">

                    <jsp:include page="pizza/mostrarPizza.jsp"></jsp:include>

                    <jsp:include page="pasta/mostrarPasta.jsp"></jsp:include>

                    <jsp:include page="lasana/mostrarLasana.jsp"></jsp:include>

                    <jsp:include page="panzerotti/mostrarPanzerotti.jsp"></jsp:include>
                    </div><!-- row -->               
                </div><!-- container -->
            </section>


        <footer class="pb-50  pt-70 pos-relative">
            <div class="pos-top triangle-bottom"></div>
            <div class="container-fluid">
                <a href="index.jsp"><img src="images/logo-white.png" alt="Logo"></a>

                <div class="pt-30">
                    <p class="underline-secondary"><b>Direcci�n</b></p>
                    <a href=https://www.google.com/maps/dir//Pizza+Bohemia.+Pizza,+Pasta,+Lasa%C3%B1as+y+Panzerotti+en+Cucuta/data=!4m8!4m7!1m0!1m5!1m1!1s0x8e66450cb8f6db53:0x5a5bd709f818a737!2m2!1d-72.4959999!2d7.893785899999999"
                       target="_blank">
                        Calle 5 # 4E-05 Barrio Popular
                        C�cuta, Norte de Santander
                        Colombia</a>
                </div>

                <div class="pt-30">
                    <p class="underline-secondary mb-10"><b>Tel�fono:</b></p>
                    <a href="tel:+57 305 812 6707 ">+57 305 812 6707 </a>
                </div>

                <ul class="icon mt-30">
                    <li><a href="https://www.facebook.com/pizzabohemiacucuta/" target="_blank"><i class="ion-social-facebook"></i></a></li>
                    <li><a href="https://www.instagram.com/pizzabohemiacucuta/" target="_blank"><i class="ion-social-instagram"></i></a></li>
                </ul>

                <p class="color-light font-9 mt-50 mt-sm-30"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    Copyright &copy;<script>document.write(new Date().getFullYear());</script> Todos los derechos reservados | <a href="login/index.html" target="_blank">Pizza Bohemia C�cuta</a>
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
            </div><!-- container -->
        </footer>

        <!-- SCIPTS -->
        <script src="plugin-frameworks/jquery-3.2.1.min.js"></script>
        <script src="plugin-frameworks/bootstrap.min.js"></script>
        <script src="plugin-frameworks/swiper.js"></script>
        <script src="common/scripts.js"></script>
    </body>
</html>