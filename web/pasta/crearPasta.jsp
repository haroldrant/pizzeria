<%-- 
    Document   : crearPasta
    Created on : 6/06/2020, 11:26:36 PM
    Author     : Harold
--%>
<%@page import="Modelo.PastaServices"%>
<%@page import="Modelo.Pasta"%>
<%@page import="org.apache.commons.io.FilenameUtils"%>
<%@page import="java.io.*,java.util.*, javax.servlet.*" %>
<%@page import="javax.servlet.ServletInputStream" %>
<%@page import="org.apache.commons.fileupload.*" %>
<%@page import="org.apache.commons.fileupload.disk.*" %>
<%@page import="org.apache.commons.fileupload.servlet.*" %>
<%@page import="org.apache.commons.io.output.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    String name = "", desc = "", archivo="";
    int pre = 0;
    String path = getServletContext().getRealPath("/")+"pasta/fotos";

    FileItemFactory factory = new DiskFileItemFactory();
    ServletFileUpload upload = new ServletFileUpload(factory);
    List items = upload.parseRequest(request);
    Iterator<FileItem> iter = items.iterator();
    while (iter.hasNext()) {
        FileItem uploaded = iter.next();

        if (uploaded.isFormField()) {
            String fieldName = uploaded.getFieldName();
            if (fieldName.equals("name")) {
                name = uploaded.getString();
            }
            if (fieldName.equals("desc")) {
                desc = uploaded.getString();
            }
            if (fieldName.equals("pre")) {
                pre = Integer.valueOf(uploaded.getString());
            }
            // No es campo de formulario, guardamos el fichero en algún sitio

        } else {    
            if (uploaded.getSize() > 0) {
            archivo = name + "." + FilenameUtils.getExtension(uploaded.getName());
            File fichero = new File(path,archivo);
            uploaded.write(fichero);
            }
        }
    }

    Pasta p = new Pasta(0,name,desc,pre,archivo);
    PastaServices pasta = new PastaServices();
    pasta.create(p);
    response.sendRedirect("../Dashboard/element-pasta.jsp");
%>