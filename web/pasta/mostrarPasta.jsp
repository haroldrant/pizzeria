<%-- 
    Document   : mostrarPasta
    Created on : 6/06/2020, 11:28:21 PM
    Author     : Harold
--%>
<%@page import="Modelo.Pasta"%>
<%@page import="Modelo.PastaServices"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<%
    PastaServices pasta = new PastaServices();
    ArrayList<Pasta> pastas = new ArrayList();
    pastas = pasta.read();
    for (Pasta pas : pastas) {
%>
<div class="col-md-6 food-menu pasta">
    <div class="sided-90x mb-30 ">
        <div class="s-left"><img class="br-3" src="pasta/fotos/<%=pas.getFoto()%>" alt="Menu Image"></div><!--s-left-->
        <div class="s-right">
            <h5 class="mb-10"><b><%=pas.getNombre()%></b><b class="color-primary float-right">$<%=pas.getPrecio()%> COP</b></h5>
            <p class="pr-70"><%=pas.getDescripcion()%> </p>
        </div><!--s-right-->
    </div><!-- sided-90x -->
</div><!-- food-menu -->
<%}%>