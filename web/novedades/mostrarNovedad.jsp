<%-- 
    Document   : mostrarNovedad
    Created on : 16/06/2020, 10:30:22 PM
    Author     : Harold
--%>

<%@page import="java.time.LocalDate"%>
<%@page import="java.time.format.DateTimeFormatter"%>
<%@page import="java.util.Locale"%>
<%@page import="Modelo.Novedad"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Modelo.NovedadServices"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="row">
<%
    int pag = 1;
    int items = 10;
    //Al momento de dar siguiente o presionar otro botón, manda como parametro "pg" con el número de página.
    if (request.getParameter("pg") != null) {
        pag = Integer.valueOf(request.getParameter("pg"));
    }
    DateTimeFormatter format = DateTimeFormatter.ofPattern("dd/MM/yy");
    NovedadServices novedad = new NovedadServices();
    ArrayList<Novedad> n = new ArrayList();
    //Obtengo los datos de la base de datos mediante el crud.
    n = novedad.read();
    //maxPag es la variable para Máximo de Páginas, se haya calculando el tamaño del arreglo de objetos entre el número de
    //Elementos por página.
    int maxPag = n.size() / items;
    //Aquí hago una operación para obtener el número de registro del que inicia.    
    int regMin = (pag - 1) * items;
    //Aquí hago una operación para obtener el número de registros máximos para mostrar en esa página.
    //Esto con el fin, de recorrer el arreglo desde el registro mínimo hasta el registro máximo.
    int regMax = pag * items;
%>
<%
    while (n.size() > regMin && regMax > regMin) {
        String fecha = n.get(regMin).getFecha().format(DateTimeFormatter.ofPattern("dd/MM/yy"));
        String f[] = fecha.split("/");
%>

<div class="col-md-6 food-menu novedades">
    <div class="sided-90x mb-30 ">
        <div class="pos-relative mb-30 pt-15">
            <div class="font-8 abs-tl p-20 bg-primary color-white">
                <h4><b><%=f[0]%></b></h4>
                <h4><b><%=f[1]%></b></h4>
                <h4><b><%=f[2]%></b></h4>
                <div class="brdr-style-1"></div>
            </div>
            <img src="novedades/fotos/<%=n.get(regMin).getFoto()%>" height="300">
        </div>
        <h4><b><%=n.get(regMin).getNombre()%></b></h4>
        <h6 class="mt-10 bg-lite-blue dplay-inl-block">
            <a class="plr-20 mtb-10" href="#"><b>Pizza Bohemia</b></a>
        </h6>
        <p class="mt-30"><%=n.get(regMin).getDescripcion()%></p>
    </div><!-- sided-90x -->
</div>

<%regMin++;
    }%>
    </div>
<div class="col-md-7 col-lg-8">
    <div class="center-text">
        <div class="block-27">
            <ul class="font-14 mb-30">
                <% //Pregunto si hay más de una página, para comenzar paginación.
                    if (maxPag >= 1) {
                        //Realizo el for para calcular el máximo de páginas.
                        for (int i = 0; i < maxPag; i++) {
                            //Si la página es igual a la página actual, muestra la etiqueta active.
                            if (i + 1 == pag) {
                %>
                <li class="active"><span><%=i + 1%></span></li>
                        <%  }//Si no, sigue mostrando las etiquetas normales con la opción para desplazarse.
                    else {%>
                <li><a href="novedades.jsp?pg=<%=i + 1%>"><%=i + 1%></a></li>
                    <%}
                        }
                    }//Si el máximo de páginas no es mayor a 1, muestra solo una página 
                    else { %>
                <li class="active"><span>1</span></li>
                    <%}
                    %>

            </ul>
        </div>
    </div>
</div>