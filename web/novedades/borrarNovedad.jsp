<%-- 
    Document   : borrarNovedad
    Created on : 16/06/2020, 11:21:27 PM
    Author     : Harold
--%>

<%@page import="java.io.File"%>
<%@page import="Modelo.Novedad"%>
<%@page import="Modelo.NovedadServices"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    int id = Integer.valueOf(request.getParameter("id"));
    NovedadServices n = new NovedadServices();
    Novedad novedad = n.readNovedad(id);
    String path = getServletContext().getRealPath("/") + "../../web/novedades/fotos";
    File file = new File(path,novedad.getFoto());
    file.delete();
    n.delete(id);
    response.sendRedirect("../Dashboard/element-novedad.jsp");
%>
