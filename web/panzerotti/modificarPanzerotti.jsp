<%-- 
    Document   : modificarPizza
    Created on : 9/06/2020, 06:39:22 PM
    Author     : Harold
--%>

<%@page import="Modelo.Panzerotti"%>
<%@page import="Modelo.PanzerottiServices"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    int id = Integer.parseInt(request.getParameter("id"));
    PanzerottiServices p = new PanzerottiServices();
    Panzerotti panzerotti = p.readPizza(id);
%>
<div class="container-fluid bcontent">
    <form action="../panzerotti/actualizarPanzerotti.jsp" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label for="text">Id</label>
            <input hidden="true" type="text" class="form-control"  id="id" name="id" value="<%=panzerotti.getId()%>">
        </div>
        <div class="form-group">
            <label for="text">Nombre de Panzerotti</label>
            <input type="text" class="form-control" id="name" name="name" value="<%=panzerotti.getNombre()%>">
        </div>
        <div class="form-group">
            <label for="text">Descripcion</label>
            <textarea class="form-control" id="desc" name="desc" rows="4"><%=panzerotti.getDescripcion()%></textarea>
        </div>
        <div class="form-group">
            <label for="text">Precio</label>
            <div class="input-group">
                <span class="input-group-addon">$</span>
                <input class="form-control" type="text" id="pre" name="pre" value="<%=panzerotti.getPrecio()%>">
                <span class="input-group-addon">.00</span>
            </div>
        </div>
        <div class="form-group">
            <label for="fileupload">Imagen</label>
            <input type="file" class="form-control-file" id="img" name="img">
        </div>
        <button type="submit" class="btn btn-primary">Actualizar</button>
        <a href="../Dashboard/element-panzerotti.jsp" role="button" class="btn btn-primary"> <span>Volver</span></a>
    </form>
</div>