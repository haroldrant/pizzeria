<%-- 
    Document   : mostrarPizzaAdmin
    Created on : 9/07/2020, 11:30:27 AM
    Author     : Harold
--%>

<%@page import="Modelo.Panzerotti"%>
<%@page import="Modelo.PanzerottiServices"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    PanzerottiServices pizza = new PanzerottiServices();
    ArrayList<Panzerotti> pizzas = new ArrayList();
    pizzas = pizza.read();
    for (Panzerotti piz : pizzas) {
%>
<div class="col-md-4">
    <!-- PANEL NO CONTROLS -->
    <div class="panel">
        <img src="../panzerotti/fotos/<%=piz.getFoto()%>" height="320">
        <div class="panel-heading">
            
            <h3 class="panel-title"><%=piz.getNombre()%></h3>
        </div>
        <div class="panel-body">
            <p><%=piz.getDescripcion()%></p>
        </div>
        <div class="panel-footer">
            <h5>$<%=piz.getPrecio()%> COP</h5>
            <a href="panzerotti-modificar.jsp?id=<%=piz.getId()%>" role="button" class="btn btn-primary">Editar</a>
            <a href="../panzerotti/borrarPanzerottis.jsp?id=<%=piz.getId()%>" role="button" class="btn btn-primary">Borrar</a>
        </div>
    </div>
    <!-- END PANEL NO CONTROLS --> 
</div>
<%}%>
